Low Resistance Optical Filament Runout Sensor
=============================================

Archived from
https://www.printables.com/model/31301-low-resistance-optical-filament-runout-sensor,
with a spacer added to mount to 20mm extrusion.  I use a rubber band instead
of the screw and spring to put tension on the arm.

