translate([0,0,-32])
%import("runout_sensor_body.stl");

difference()
{
    translate([-20,-12,-10])
        cube([40,12,20]);
    translate([-12.5,1,0])
    rotate([90,0,0])
        cylinder(d=5.5, h=20, $fn=60);
    translate([12.5,1,0])
    rotate([90,0,0])
        cylinder(d=5.5, h=20, $fn=60);
}